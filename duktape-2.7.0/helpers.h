#pragma once

#include "duktape.h"

class Console {
public:
    explicit Console(duk_context *ctx): ctx_(ctx) {
        duk_push_global_object(ctx);
        auto obj_idx = duk_push_bare_object(ctx);
        duk_push_c_function(ctx, log_text, 1);
        duk_put_prop_string(ctx, obj_idx, "log");
        duk_push_c_function(ctx, log_stackframe, 0);
        duk_put_prop_string(ctx, obj_idx, "logStack");
        duk_put_prop_string(ctx, obj_idx - 1, "console");
        duk_pop(ctx); // global stash
    }

    void logStack() {
        log_stackframe(ctx_);
    }

private:
    duk_context *ctx_;

   static duk_ret_t log_stackframe(duk_context *ctx) {
        duk_push_context_dump(ctx);
        printf("stack: %s\n", duk_to_string(ctx, -1));
        duk_pop(ctx);
        return 0;
    }

    static duk_ret_t log_text(duk_context *ctx) {
        printf("%s\n", duk_to_string(ctx, 0));
        return 0;
    }
};

