#include <memory>
#include <duktape.h>
#include "duktape-2.7.0/helpers.h"

int main() {
    duk_context *ctx = duk_create_heap_default();
    auto console = std::make_unique<Console>(ctx);
    duk_push_global_object(ctx);
    duk_eval_string_noresult(ctx, "console.log(my_function();)"); // should log 4

//    duk_get_prop_string(ctx, -1, "myobject");
//    console->logStack();
//    auto myobject = duk_get_int(ctx, -1);
//    printf("myobject value is %i\n", myobject);
//    duk_pop(ctx);
//    duk_pop(ctx);
//    console->logStack();
    duk_destroy_heap(ctx);
    return 0;
}
